package ruialmeida.minderachallenge.UI.Fragment.HomeFragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ruialmeida.minderachallenge.R;
import ruialmeida.minderachallenge.Service.DummyData;
import ruialmeida.minderachallenge.UI.Adapter.HorizontalRecyclerViewAdapter;


public class EventsFragment extends Fragment {

    private OnItemSelectedListener listener;

    //Recycler Views
    private RecyclerView graduateProgramRecyclerView;
    private RecyclerView openDayRecyclerView;
    private RecyclerView meetMinderaRecyclerView;

    //Layout Managers
    private LinearLayoutManager openDayLayoutManager;
    private LinearLayoutManager graduateProgramLayoutManager;
    private LinearLayoutManager meetMinderaLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_events, container, false);

        setupOpenDayRecyclerView(rootView);
        setupGraduateProgram(rootView);
        setupMeetMinderaCodeAndCulture(rootView);

        return rootView;
    }

    /**
     * Creates the horizontal scrolling cards for the open days
     * using the Recycler View Adapter.
     *
     * @param rootView
     */
    private void setupOpenDayRecyclerView(View rootView) {
        openDayLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        graduateProgramRecyclerView = rootView.findViewById(R.id.fragment_event_open_day_recycler_view);
        HorizontalRecyclerViewAdapter adapter = new HorizontalRecyclerViewAdapter(getContext(), DummyData.getOpenDays());

        graduateProgramRecyclerView.setLayoutManager(openDayLayoutManager);
        graduateProgramRecyclerView.setAdapter(adapter);
    }

    /**
     * Creates the horizontal scrolling cards for the graduate program
     * using the Recycler View Adapter.
     *
     * @param rootView
     */
    private void setupGraduateProgram(View rootView) {
        graduateProgramLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        openDayRecyclerView = rootView.findViewById(R.id.fragment_event_graduate_program_recycler_view);
        HorizontalRecyclerViewAdapter adapter = new HorizontalRecyclerViewAdapter(getContext(), DummyData.getGraduateProgram());

        openDayRecyclerView.setLayoutManager(graduateProgramLayoutManager);
        openDayRecyclerView.setAdapter(adapter);
    }

    /**
     * Creates the horizontal scrolling cards for the mindera code and culture section
     * using the Recycler View Adapter.
     *
     * @param rootView
     */
    private void setupMeetMinderaCodeAndCulture(View rootView) {
        meetMinderaLayout = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        meetMinderaRecyclerView = rootView.findViewById(R.id.fragment_event_meet_mindera_recycler_view);
        HorizontalRecyclerViewAdapter adapter = new HorizontalRecyclerViewAdapter(getContext(), DummyData.getMeetMindera());

        meetMinderaRecyclerView.setLayoutManager(meetMinderaLayout);
        meetMinderaRecyclerView.setAdapter(adapter);
    }

    /**
     * Interface that defines communication between the fragment and
     * the main activity.
     */
    public interface OnItemSelectedListener {
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnItemSelectedListener) listener = (OnItemSelectedListener) context;
        else throw new ClassCastException();
    }

}
