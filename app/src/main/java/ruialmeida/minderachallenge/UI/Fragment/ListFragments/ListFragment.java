package ruialmeida.minderachallenge.UI.Fragment.ListFragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import ruialmeida.minderachallenge.Model.ItemList;
import ruialmeida.minderachallenge.R;
import ruialmeida.minderachallenge.UI.Adapter.SimpleListRecyclerViewAdapter;
import ruialmeida.minderachallenge.UI.ListActivity;

public class ListFragment extends Fragment {

    private ListFragment.OnItemSelectedListener listener;

    private ArrayList<ItemList> items;
    private String name;

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_list, container, false);

        if (getArguments() != null) {
            items = (ArrayList<ItemList>) getArguments().getSerializable("ListItems");
            name = getArguments().getString("ListName");
            ((ListActivity) getActivity()).getSupportActionBar().setTitle(name);
        }


        setupList(rootView);
        return rootView;
    }

    private void setupList(View rootView) {
        recyclerView = rootView.findViewById(R.id.fragment_event_list_recyclerview);
        layoutManager = new LinearLayoutManager(rootView.getContext(), LinearLayoutManager.VERTICAL, false);
        SimpleListRecyclerViewAdapter adapter = new SimpleListRecyclerViewAdapter(rootView.getContext(), items);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }


    /**
     * Interface that defines communication between the fragment and
     * the main activity.
     */
    public interface OnItemSelectedListener {
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ListFragment.OnItemSelectedListener)
            listener = (ListFragment.OnItemSelectedListener) context;
        else throw new ClassCastException();
    }
}
