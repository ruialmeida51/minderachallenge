package ruialmeida.minderachallenge.UI.Fragment.HomeFragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ruialmeida.minderachallenge.R;
import ruialmeida.minderachallenge.Service.DummyData;
import ruialmeida.minderachallenge.UI.Adapter.FancyListRecyclerViewAdapter;

public class VacanciesFragment extends Fragment {

    private OnItemSelectedListener listener;
    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_vacancies, container, false);


        setupFancyListView(rootView);

        return rootView;
    }

    private void setupFancyListView(View rootView) {
        recyclerView = rootView.findViewById(R.id.fragment_vacancies_list_recyclerview);
        layoutManager = new LinearLayoutManager(rootView.getContext(), LinearLayoutManager.VERTICAL, false);
        FancyListRecyclerViewAdapter adapter = new FancyListRecyclerViewAdapter(rootView.getContext(), DummyData.getVacancies());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    /**
     * Interface that defines communication between the fragment and
     * the main activity.
     */
    public interface OnItemSelectedListener {
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnItemSelectedListener) listener = (OnItemSelectedListener) context;
        else throw new ClassCastException();
    }
}