package ruialmeida.minderachallenge.UI.Fragment.ListFragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import ruialmeida.minderachallenge.Model.ItemListDescription;
import ruialmeida.minderachallenge.R;
import ruialmeida.minderachallenge.UI.Adapter.GridRecyclerViewAdapter;
import ruialmeida.minderachallenge.UI.ListActivity;

public class DescriptionFragment extends Fragment {

    private OnItemSelectedListener listener;

    private RecyclerView recyclerView;

    private ArrayList<ItemListDescription> items;
    private String name;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_description, container, false);

        if (getArguments() != null) {
            items = (ArrayList<ItemListDescription>) getArguments().getSerializable("ListItemsDescription");
            name = getArguments().getString("ListItemsDescriptionName");
            ((ListActivity) getActivity()).getSupportActionBar().setTitle(name);
        }


        setupGrid(rootView);

        return rootView;
    }

    private void setupGrid(View rootView) {

        recyclerView = rootView.findViewById(R.id.fragment_event_list_description_recyclerview);
        GridRecyclerViewAdapter adapter = new GridRecyclerViewAdapter(rootView.getContext(), items);

        recyclerView.setLayoutManager(new GridLayoutManager(rootView.getContext(), 2));
        recyclerView.setAdapter(adapter);
    }

    /**
     * Interface that defines communication between the fragment and
     * the main activity.
     */
    public interface OnItemSelectedListener {
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnItemSelectedListener) listener = (OnItemSelectedListener) context;
        else throw new ClassCastException();
    }
}
