package ruialmeida.minderachallenge.UI.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import ruialmeida.minderachallenge.Model.Item;
import ruialmeida.minderachallenge.R;
import ruialmeida.minderachallenge.UI.ListActivity;

public class HorizontalRecyclerViewAdapter extends RecyclerView.Adapter<HorizontalRecyclerViewAdapter.ViewHolder> {

    private ArrayList<Item> items;
    private Context context;

    public HorizontalRecyclerViewAdapter(Context context, ArrayList<Item> names) {
        this.items = names;
        this.context = context;
    }

    @Override
    public HorizontalRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_horizontal_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.description.setText(items.get(position).getName());
        holder.image.setImageResource(items.get(position).getDrawable());
    }

    @Override
    public int getItemCount() {
        if (this.items == null) return 0;
        return this.items.size();
    }

    /**
     * Nested class that holds the data in memory
     */
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        AppCompatImageView image;
        TextView description;


        public ViewHolder(View itemView) {
            super(itemView);

            context = itemView.getContext();

            image = itemView.findViewById(R.id.fragment_event_image);
            description = itemView.findViewById(R.id.fragment_event_imageText);

            itemView.setClickable(true);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            final Intent intent = new Intent(context, ListActivity.class);
            intent.putExtra("ListItems", items.get(getAdapterPosition()).getList());
            intent.putExtra("ListName", items.get(getAdapterPosition()).getSection() + "_" + items.get(getAdapterPosition()).getName());
            context.startActivity(intent);
        }
    }
}
