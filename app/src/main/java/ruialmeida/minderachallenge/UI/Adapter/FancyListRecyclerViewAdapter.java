package ruialmeida.minderachallenge.UI.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import ruialmeida.minderachallenge.Model.Vacancy;
import ruialmeida.minderachallenge.R;

public class FancyListRecyclerViewAdapter extends RecyclerView.Adapter<FancyListRecyclerViewAdapter.ViewHolder> {

    private Context context;
    private ArrayList<Vacancy> vacancies;

    public FancyListRecyclerViewAdapter(Context context, ArrayList<Vacancy> vacancies) {
        this.context = context;
        this.vacancies = vacancies;
    }

    @Override
    public FancyListRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        return new FancyListRecyclerViewAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_fancy_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Vacancy vac = vacancies.get(position);
        holder.job.setText(vac.getJob());
        holder.description.setText(vac.getDescription());
        holder.pay.setText("" + vac.getPay());
    }

    @Override
    public int getItemCount() {
        if (this.vacancies == null) return 0;
        return this.vacancies.size();
    }

    /**
     * Nested class that holds the data in memory
     */
    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView job;
        TextView description;
        TextView pay;

        public ViewHolder(View itemView) {
            super(itemView);

            context = itemView.getContext();

            job = itemView.findViewById(R.id.fragment_vacancies_list_textview_title);
            description = itemView.findViewById(R.id.fragment_vacancies_list_textview_title_desc);
            pay = itemView.findViewById(R.id.fragment_vacancies_list_textview_title_pay);
        }
    }
}
