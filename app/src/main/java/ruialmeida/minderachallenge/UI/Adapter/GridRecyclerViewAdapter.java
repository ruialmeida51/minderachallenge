package ruialmeida.minderachallenge.UI.Adapter;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import ruialmeida.minderachallenge.Model.ItemListDescription;
import ruialmeida.minderachallenge.R;

public class GridRecyclerViewAdapter extends RecyclerView.Adapter<GridRecyclerViewAdapter.ViewHolder> {

    private ArrayList<ItemListDescription> items;
    private Context context;

    public GridRecyclerViewAdapter(Context context, ArrayList<ItemListDescription> items) {
        this.items = items;
        this.context = context;
    }

    @Override
    public GridRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        return new GridRecyclerViewAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_grid_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.description.setText(items.get(position).getName());
        holder.image.setImageResource(items.get(position).getDrawable());
    }

    @Override
    public int getItemCount() {
        if (this.items == null) return 0;
        return this.items.size();
    }

    /**
     * Nested class that holds the data in memory
     */
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView description;
        AppCompatImageView image;

        public ViewHolder(View itemView) {
            super(itemView);

            context = itemView.getContext();

            description = itemView.findViewById(R.id.fragment_description_imageText);
            image = itemView.findViewById(R.id.fragment_description_image);

            itemView.setClickable(true);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            // Do nothing

        }
    }
}
