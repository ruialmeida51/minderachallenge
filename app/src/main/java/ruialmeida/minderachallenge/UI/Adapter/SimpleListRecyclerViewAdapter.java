package ruialmeida.minderachallenge.UI.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import ruialmeida.minderachallenge.Model.ItemList;
import ruialmeida.minderachallenge.R;
import ruialmeida.minderachallenge.UI.Fragment.ListFragments.DescriptionFragment;
import ruialmeida.minderachallenge.UI.ListActivity;
import ruialmeida.minderachallenge.Utils.FragmentUtils;

public class SimpleListRecyclerViewAdapter extends RecyclerView.Adapter<SimpleListRecyclerViewAdapter.ViewHolder> {

    private ArrayList<ItemList> items;
    private Context context;

    public SimpleListRecyclerViewAdapter(Context context, ArrayList<ItemList> items) {
        this.items = items;
        this.context = context;
    }

    @Override
    public SimpleListRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        return new SimpleListRecyclerViewAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_simple_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.name.setText(items.get(position).getName());
    }

    @Override
    public int getItemCount() {
        if (this.items == null) return 0;
        return this.items.size();
    }

    /**
     * Nested class that holds the data in memory
     */
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView name;

        public ViewHolder(View itemView) {
            super(itemView);

            context = itemView.getContext();

            name = itemView.findViewById(R.id.fragment_event_list_textview);

            itemView.setClickable(true);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            // Send data to description fragment

            Bundle bundle = new Bundle();
            bundle.putSerializable("ListItemsDescription", items.get(getAdapterPosition()).getDescriptions());
            bundle.putString("ListItemsDescriptionName", items.get(getAdapterPosition()).getName());

            DescriptionFragment descriptionFragment = new DescriptionFragment();
            descriptionFragment.setArguments(bundle);

            FragmentUtils.setFragment(R.id.fragment_list, descriptionFragment, (ListActivity) context, true);
        }
    }
}
