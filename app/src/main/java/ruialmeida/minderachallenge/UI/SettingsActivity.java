package ruialmeida.minderachallenge.UI;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import ruialmeida.minderachallenge.R;

public class SettingsActivity extends AppCompatActivity  implements View.OnClickListener {

    private SwitchCompat pushNotificationSwitch;
    private boolean pushNotifications;
    private SharedPreferences mPreferences;
    private SharedPreferences.Editor mEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        Toolbar toolbar = findViewById(R.id.settings_toolbar);
        setSupportActionBar(toolbar);

        pushNotificationSwitch = findViewById(R.id.fragment_settings_push_notifications_switch);
        pushNotificationSwitch.setOnClickListener(this);

        mPreferences = getSharedPreferences(HomeActivity.PREFS_NAME, 0);
        pushNotifications = mPreferences.getBoolean("disablePushNotifications", false);
        pushNotificationSwitch.setChecked(pushNotifications);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("Settings");
        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }

        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_settings_push_notifications_switch:
                mEditor = mPreferences.edit();
                mEditor.putBoolean("disablePushNotifications", !pushNotifications);
                mEditor.commit();
                break;
        }
    }
}
