package ruialmeida.minderachallenge.UI;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import ruialmeida.minderachallenge.R;
import ruialmeida.minderachallenge.UI.Fragment.HomeFragments.EventsFragment;
import ruialmeida.minderachallenge.UI.Fragment.HomeFragments.VacanciesFragment;
import ruialmeida.minderachallenge.Utils.FragmentUtils;
import ruialmeida.minderachallenge.Utils.NotificationSender;

public class HomeActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener,
        EventsFragment.OnItemSelectedListener,
        VacanciesFragment.OnItemSelectedListener,
        SearchView.OnQueryTextListener {

    public static final String PREFS_NAME = "SharedPreferences";
    private SharedPreferences mPreferences;
    private SharedPreferences.Editor mEditor;
    private boolean pushNotifications;
    private boolean doubleBackToExitPressedOnce = false;

    private Toolbar toolbar;
    private TabLayout nav_toolbar;
    private FloatingActionButton fab;
    private DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        toolbar = findViewById(R.id.toolbar);
        nav_toolbar = findViewById(R.id.nav_toolbar);
        drawer = findViewById(R.id.drawer_layout);
        setSupportActionBar(toolbar);

        //Setup shared preferences
        mPreferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        mEditor = mPreferences.edit();

        setupTabLayout();
        setupFabButton();
        setupNavigationView();
    }

    @Override
    public void onBackPressed() {
        // If the drawer is open, the back button will close it.
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

            // Ask user to press back button twice before closing the app
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click 'BACK' again to exit.", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }

    }




    /* ======================================================
                       SETUP THE UI VIEWS
       ====================================================== */


    /**
     * Sets up the tab layout:
     * First, defines a default fragment for the FrameLayout.
     * Then, adds a selected listener to for each tab item.
     */
    protected void setupTabLayout() {
        // Set default fragment to events.
        FragmentUtils.setFragment(R.id.fragmentcontainer, new EventsFragment(), this, false);

        //Tab listener logic
        nav_toolbar.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tabListener(tab, false);
                getSupportActionBar().setTitle("Meet Mindera");
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {/*Do Nothing*/}

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                tabListener(tab, false);
                getSupportActionBar().setTitle("Meet Mindera");
            }
        });
    }

    /**
     * Sets up a FAB button that sends a simple Snackbar text.
     * Doesn't do a lot, just a small message for anyone who uses the application.
     */
    protected void setupFabButton() {
        fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pushNotifications = mPreferences.getBoolean("disablePushNotifications", false);

                if (!pushNotifications) {
                    Snackbar.make(view, R.string.fab_message_enabled, Snackbar.LENGTH_SHORT).setAction("Action", null).show();
                    NotificationSender.sendNotification(HomeActivity.this);
                } else {
                    Snackbar.make(view, R.string.fab_message_disabled, Snackbar.LENGTH_SHORT).setAction("Action", null).show();
                }
            }
        });
    }

    /**
     * Sets up up the navigation view side panel.
     * It holds two more options for navigating: About & Settings.
     */
    protected void setupNavigationView() {
        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }


    /**
     * Creates the options menu, on the top right corner
     * of the activity.
     *
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }




    /* ======================================================
                       UI LISTENER ACTIONS
       ====================================================== */


    /**
     * Tab Items in a tab layout actions.
     *
     * @param tab
     */
    protected void tabListener(TabLayout.Tab tab, boolean addToBackStack) {
        switch (tab.getPosition()) {

            // Events
            case 0:
                fab.show();
                FragmentUtils.setFragment(R.id.fragmentcontainer, new EventsFragment(), this, addToBackStack);
                break;

            // Vacancy
            case 1:
                fab.show();
                FragmentUtils.setFragment(R.id.fragmentcontainer, new VacanciesFragment(), this, addToBackStack);
                break;
        }
    }

    /**
     * Navigation drawer actions
     *
     * @param item - the selected icon
     * @return
     */
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {

            case R.id.nav_settings:
                intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_about:
                intent = new Intent(this, AboutActivity.class);
                startActivity(intent);
                break;

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    /**
     * Listener for the options menu, at the top right
     * corner of the activity.
     *
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            Toast.makeText(this, "Clicked search. It's not imeplemented yet, though!", Toast.LENGTH_SHORT);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        Toast.makeText(this, "What", Toast.LENGTH_SHORT);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        return false;
    }
}
