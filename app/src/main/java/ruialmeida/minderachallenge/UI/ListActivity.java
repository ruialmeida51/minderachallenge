package ruialmeida.minderachallenge.UI;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import java.util.ArrayList;

import ruialmeida.minderachallenge.Model.ItemList;
import ruialmeida.minderachallenge.R;
import ruialmeida.minderachallenge.UI.Fragment.ListFragments.DescriptionFragment;
import ruialmeida.minderachallenge.UI.Fragment.ListFragments.ListFragment;
import ruialmeida.minderachallenge.Utils.FragmentUtils;

public class ListActivity extends AppCompatActivity implements
        ListFragment.OnItemSelectedListener,
        DescriptionFragment.OnItemSelectedListener {

    private ArrayList<ItemList> items;
    private String name;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_items);

        items = (ArrayList<ItemList>) getIntent().getSerializableExtra("ListItems");
        name = (String) getIntent().getSerializableExtra("ListName");


        Toolbar toolbar = findViewById(R.id.list_items_toolbar);
        setSupportActionBar(toolbar);

        // Send data to fragment
        Bundle bundle = new Bundle();
        bundle.putSerializable("ListItems", items);
        bundle.putSerializable("ListName", name);
        ListFragment listFragment = new ListFragment();
        listFragment.setArguments(bundle);
        FragmentUtils.setFragment(R.id.fragment_list, listFragment, this, false);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(name);
        }

    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }

        return true;
    }



}
