package ruialmeida.minderachallenge.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class Item implements Serializable {

    private String name;
    private String section;
    private int drawableResource;
    private ArrayList<ItemList> list;

    public Item(String name, int drawableResource, ArrayList<ItemList> list, String section)
    {
        this.name = name;
        this.drawableResource = drawableResource;
        this.list = list;
        this.section = section;
    }

    public String getName()
    {
        return this.name;
    }

    public String getSection()
    {
        return this.section;
    }

    public int getDrawable()
    {
        return this.drawableResource;
    }

    public ArrayList<ItemList> getList() { return this.list; }
}
