package ruialmeida.minderachallenge.Model;

public class Vacancy {

    private String job, description;
    double pay;

    public Vacancy(String job, String description, double pay) {
        this.job = job;
        this.description = description;
        this.pay = pay;
    }

    public String getJob() { return this.job; }
    public String getDescription() { return this.description; }
    public double getPay() { return this.pay; }
}
