package ruialmeida.minderachallenge.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class ItemList implements Serializable {

    private String name;
    private ArrayList<ItemListDescription> descriptions;

    public ItemList(String name, ArrayList<ItemListDescription> descriptions)
    {
        this.name = name;
        this.descriptions = descriptions;
    }

    public String getName()
    {
        return this.name;
    }


    public ArrayList<ItemListDescription> getDescriptions() { return this.descriptions; }
}
