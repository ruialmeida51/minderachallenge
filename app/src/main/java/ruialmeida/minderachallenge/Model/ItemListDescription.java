package ruialmeida.minderachallenge.Model;

import java.io.Serializable;

public class ItemListDescription implements Serializable {

    private String name;
    private int drawableResource;

    public ItemListDescription(String name, int drawableResource)
    {
        this.name = name;
        this.drawableResource = drawableResource;
    }

    public String getName()
    {
        return this.name;
    }

    public int getDrawable()
    {
        return this.drawableResource;
    }
}