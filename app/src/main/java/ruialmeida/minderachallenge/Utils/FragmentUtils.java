package ruialmeida.minderachallenge.Utils;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import ruialmeida.minderachallenge.R;

public class FragmentUtils {

    /**
     * Changes the currently displayed fragment on a certain activity.
     *
     * @param fragment - fragment to set as active
     * @param activityCompat - the activity holding the fragment
     */
    public static void setFragment(int container, Fragment fragment, AppCompatActivity activityCompat, boolean addToBackStack) {
        FragmentTransaction fragmentTransaction = activityCompat.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(container, fragment);
        if (addToBackStack) fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}
