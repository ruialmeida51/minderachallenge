package ruialmeida.minderachallenge.Utils;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

import ruialmeida.minderachallenge.R;

public class NotificationSender {

    public static void sendNotification(Context context) {
        String channelId = "HomeActivityChannel-01";
        String channelName = "HomeActivityChannel";
        int importance = NotificationManager.IMPORTANCE_HIGH;
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelId);
        mBuilder.setSmallIcon(R.drawable.list_icon);
        mBuilder.setContentTitle("Meet Mindera");
        mBuilder.setContentText("Thank you for using the app!");
        mBuilder.setChannelId(channelId);
        mBuilder.setAutoCancel(true);
        mBuilder.setWhen(0);

        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(channelId, channelName, importance);
            mNotificationManager.createNotificationChannel(mChannel);
        }

        mNotificationManager.notify(1, mBuilder.build());
    }

}
