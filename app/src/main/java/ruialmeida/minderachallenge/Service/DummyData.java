package ruialmeida.minderachallenge.Service;

import java.util.ArrayList;

import ruialmeida.minderachallenge.Model.Item;
import ruialmeida.minderachallenge.Model.ItemList;
import ruialmeida.minderachallenge.Model.ItemListDescription;
import ruialmeida.minderachallenge.Model.Vacancy;
import ruialmeida.minderachallenge.R;

public class DummyData {

    public static ArrayList<Item> getOpenDays() {
        ArrayList<Item> openDays = new ArrayList<>();
        ArrayList<ItemList> openDaysList = new ArrayList<>();
        ArrayList<ItemListDescription> openDaysListDescription = new ArrayList<>();

        // Create Item List Descriptions
        for (int j = 0; j < 10; j++)
            openDaysListDescription.add(new ItemListDescription("Description " + (j + 1), R.color.colorPrimaryDark));


        // Create Items
        for (int i = 0; i < 10; i++) {
            openDaysList.add(new ItemList("List Item " + (i + 1), openDaysListDescription));
            openDays.add(new Item("Day " + (i + 1), R.color.colorPrimaryDark, openDaysList, "Open Day '18"));
        }

        return openDays;
    }

    public static ArrayList<Item> getGraduateProgram() {

        ArrayList<Item> graduatePrograms = new ArrayList<>();
        ArrayList<ItemList> graduateProgramsList = new ArrayList<>();
        ArrayList<ItemListDescription> graduateProgramsDescription = new ArrayList<>();

        // Create Item List Descriptions
        for (int j = 0; j < 10; j++)
            graduateProgramsDescription.add(new ItemListDescription("Description " + (j + 1), R.color.colorPrimaryDark));


        // Create Items
        for (int i = 0; i < 10; i++) {
            graduateProgramsList.add(new ItemList("List Item " + (i + 1), graduateProgramsDescription));
            graduatePrograms.add(new Item("", R.color.colorPrimaryDark, graduateProgramsList, "Graduate Program"));
        }

        return graduatePrograms;
    }

    public static ArrayList<Item> getMeetMindera() {
        ArrayList<Item> meetMindera = new ArrayList<>();
        ArrayList<ItemList> meetMinderaList = new ArrayList<>();
        ArrayList<ItemListDescription> meetMinderaListDescription = new ArrayList<>();

        // Create Item List Descriptions
        for (int j = 0; j < 10; j++)
            meetMinderaListDescription.add(new ItemListDescription("Description " + (j + 1), R.color.colorPrimaryDark));


        // Create Items
        for (int i = 0; i < 10; i++) {
            meetMinderaList.add(new ItemList("List Item " + (i + 1), meetMinderaListDescription));
            meetMindera.add(new Item("", R.color.colorPrimaryDark, meetMinderaList, "Meet Mindera"));
        }

        return meetMindera;
    }

    public static ArrayList<Vacancy> getVacancies()
    {
        ArrayList<Vacancy> vacancies = new ArrayList<>();

        vacancies.add(new Vacancy(".NET Developer", "Back-End Developer.", 1200));
        vacancies.add(new Vacancy("Full Stack Developer", "Full Stack Node.JS", 1300));
        vacancies.add(new Vacancy("Android Developer", "We're hiring android developers.", 1200));
        vacancies.add(new Vacancy("Product Manager", "We're hiring a product manager.", 1100));
        vacancies.add(new Vacancy("Office Cleaner", "John Doe never cleans up after himself..", 750));
        vacancies.add(new Vacancy("Lieutenant Office Cleaner", "Every foot soldier needs a commander.", 950));
        vacancies.add(new Vacancy("Srgt. Pancake Flipper", "The devs need food! They're starving!", 1000));
        vacancies.add(new Vacancy("Master Coffee Deployer", "Coffee continuous integration expert.", 1200));
        vacancies.add(new Vacancy("QA Tester", "It was working when I tested it..", 1000));
        vacancies.add(new Vacancy("Arctic Firefighter", "\"There's a fire!\" Yeah, right, Dave.", 2500));
        vacancies.add(new Vacancy("Business Analyst", "The numbers Mason, what do they mean?!", 1200));
        vacancies.add(new Vacancy("HTML/CSS Programmer", "It's a real programming language, I swear!", 1450));

        return vacancies;
    }
}
